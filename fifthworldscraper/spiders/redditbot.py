# -*- coding: utf-8 -*-
import scrapy

'''
Scrapy bot that extracts all media urls from any subreddit defined in 'start_urls'
saves them in fifthworldgifs.csv

 -> to run `scrapy crawl redditbot`

Run this first and then run downloadgifs.py

@author Daniel Pekevski
'''

class RedditbotSpider(scrapy.Spider):

    name = 'redditbot'
    allowed_domains = ['www.reddit.com/']

    # put your subreddits here (by page if)
    start_urls = [
    	'http://www.reddit.com/r/fifthworldgifs//',
    	'https://www.reddit.com/r/FifthWorldGifs/?count=25&after=t3_1koqxn',
    	'https://www.reddit.com/r/SpaceGfys/',
    	'https://www.reddit.com/r/SpaceGfys/?count=25&after=t3_3dd70g',
    	'https://www.reddit.com/r/SpaceGfys/?count=50&after=t3_2bj23y'
    ]

    def parse(self, response):
        for url in response.css('div::attr(data-url)').extract():
            yield {'gif_url': url}
