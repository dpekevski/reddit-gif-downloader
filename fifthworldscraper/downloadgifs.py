import urllib
import urlparse
import csv
import os
import requests
import shutil
import time
import moviepy.editor as mp

'''
Downloads gifs from media urls in .csv scraped by scrapy from certain sub reddits.
Once downloading has finished (pretty slow atm) we convert every gif
in the directory into a mp4 that we can then do anything with in a video editing
software.

TODO: look into stiching mp4's progmatically (but never will actually do)
TODO: make retry recursive
TODO: make printing dynamic with a method based on result code/retry attempt.

@author Daniel Pekevski
'''

# directory where you want to do tings
directory_to_save = "/Volumes/Daniel/space_gifs"

# csv file we werkin' with
csv_file = "fifthworldgifs.csv"


'''
Extracts an extension from a given url
eg. test.html -> html
eg. www.somegif.gif -> gif
'''
def extract_extension(url):
	path = urlparse.urlparse(url).path
	ext = os.path.splitext(path)[1]
	return ext

'''
Converts a GFYCAT url into a media url where the media is served from.
Usually of pattern:

http://zippy.gfycat/<random>.mp4
http://giant.gfycat/<random>.mp4
http://fat.gfycat/<random>.mp4

handles all input urls and outputs the following:
http://www.gfycat.com/<random> => http://giant.gfycat.com/<random>.mp4
http://zippy.gfycat/<random>.mp4 => http://giant.gfycat.com/<random>.mp4
http://giant.gfycat/<random>.mp4 => http://zippy.gfycat.com/<random>.mp4
http://fat.gfycat/<random>.mp4 => http://zippy.gfycat.com/<random>.mp4

Also contains a retry mechanism to convert urls to giant -> zippy -> fat
for a 403 forbidden response from requests.

TODO but never will: make this recursive.

'''
def convert_gfycat_url(gfycat_url, retry):
	# print "    (debug) " + "converting gfycat url [" + gfycat_url + "]..."

	subtract_www_start = subtract_www_end = 0
	subtract_added = 0

	https = 'https://'
	http = 'http://'
	www = 'www.'
	mp4 = '.mp4'

	gfycat = 'gfycat'

	if gfycat_url.endswith('#'):
		gfycat_url = gfycat_url[:-1]

	if gfycat_url.startswith(https):
		x = len(https)

	if gfycat_url.startswith(http):
		x = len(http)

	if www in gfycat_url:
		subtract_www_end = len(www)

	if "gfycat" in gfycat_url:
		added = 'giant.'

	if "zippy." in gfycat_url:
		added = "giant."
		subtract_added = len('zippy.')

	if "giant." in gfycat_url:
		if retry is 1:
			added = "fat."
			subtract_added = len('giant.')
		else:
			added = "zippy."
			subtract_added = len('giant.')

	if "fat." in gfycat_url:
		if retry is 2:
			added = "zippy."
			subtract_added = len('fat.')
		else:
			added = "fat."
			subtract_added = len('fat.')

	protocol = gfycat_url[:x + subtract_www_start]
	directory = gfycat_url[x + subtract_www_end + subtract_added:]
	url = protocol + added + directory

	url = url if url.endswith(mp4) else url + mp4

	# print "protocol: " + protocol
	# print "added: " + added
	# print "directory: " + directory
	# print "ext: " + mp4
	# print "final url: " + url
	# print ""

	return url

'''
Converts gifv urls to mp4 (just change the extension)
'''
def convert_gifv_url(gifv_url):
	# print "    (debug) " + "converting gifv url [" + gifv_url + "]..."
	x = len("gifv")
	url = gifv_url[:-x] + 'mp4'
	return url

'''
Converts imgur urls with no media extension to contain one
'''
def convert_missing_imgur(imgur_url):
	# print "    (debug) " + "converting imgur url [" + imgur_url + "]..."

	if imgur_url.endswith('#'):
		return imgur_url[:-1] + '.gif'
	elif imgur_url.endswith('mp4'):
		return imgur_url[:-3] + 'gif'
	elif imgur_url.endswith('jpg'):
		return imgur_url[:-3] + 'gif'

	return imgur_url + ".gif"

'''
Switch to indicate what to do with the inputted url and convert 
it to a url where the media is served from
'''
def sanitize(url):
	if "gfycat" in url:
		url = convert_gfycat_url(url, 0)

	if "gifv" in url:
		url = convert_gifv_url(url)

	if "imgur" in url and not url.endswith(".gif"):
		url = convert_missing_imgur(url)

	return url

'''
Handles looping through the .csv file of urls, parsing them and then
downloading them with the requests library.

Also handles certain responses.

TODO: split the retry section up for gfycat urls to be recursive.
'''
def main():
	with open(csv_file, 'rb') as csvfile:
		spamreader = csv.reader(csvfile, delimiter=' ', quotechar='|')
		index = 0

		for row in spamreader:
			url = row[0]

			if url.startswith('http'):
				print "==> " + str(index)

				url = sanitize(url)
				ext = extract_extension(url)
				save = os.path.join(directory_to_save, str(index) + ext)

				try:
					# make the request to download the media
					r = requests.get(url, stream=True)

					# on a successful response copy the file into the directory_to_save
					if r.status_code == 200:
						with open(save, 'wb') as f:
							r.raw.decode_content = True
							shutil.copyfileobj(r.raw, f)
						print "[SUCCESS] [200] downloaded [" + url + "] ..."

					# on a 404 - the content no longer exists so lets just do nothing
					elif r.status_code == 404:
						print "[FAILED] [404] failed to download [" + url + "] ..."

					# on a 403 - if its a gfycat url we try again with a newly generated url
					elif r.status_code == 403:
						

						# RETRY 1
						if "gfycat" in url:
							print "     [RETRY] [403] forbidden to download [" + url + "] ..."

							url = convert_gfycat_url(url, 1)
							ext = extract_extension(url)
							save = os.path.join(directory_to_save, str(index) + ext)

							try:
								r = requests.get(url, stream=True)

								if r.status_code == 200:
									with open(save, 'wb') as f:
										r.raw.decode_content = True
										shutil.copyfileobj(r.raw, f)
									print "        [SUCCESS] [200] downloaded [" + url + "] ..."

								elif r.status_code == 404:
									print "        [RETRY] [FAILED] [404] failed to download [" + url + "] ..."
								

								# RETRY 2
								elif r.status_code == 403:
									# try again with zippy last go omg...

									if "gfycat" in url:
										print "         [RETRY] [403] forbidden to download [" + url + "] ..."

										url = convert_gfycat_url(url, 2)
										ext = extract_extension(url)
										save = os.path.join(directory_to_save, str(index) + ext)

										try:
											r = requests.get(url, stream=True)

											if r.status_code == 200:
												with open(save, 'wb') as f:
													r.raw.decode_content = True
													shutil.copyfileobj(r.raw, f)
												print "            [SUCCESS] [200] downloaded [" + url + "] ..."

											elif r.status_code == 404:
												print "            [RETRY] [FAILED] [404] failed to download [" + url + "] ..."
											
											# ok we give up we cant get the freakin gif/mp4 from gfycat.
											elif r.status_code == 403:
												print "            [RETRY] [FAIL] [403] forbidden to download [" + url + "] ..."
											
											elif r.status_code == 500:
												print "            [RETRY] [FAILED] [500] failed to download [" + url + "] server down..."
										except:
											print "            [EPIC FAIL] [exception] failed to download [" + url + "] ..."

									else:
										print "        [FAIL] [403] forbidden to download [" + url + "] ..."
								
								elif r.status_code == 500:
									print "        [RETRY] [FAILED] [500] failed to download [" + url + "] server down..."
							except:
								print "        [EPIC FAIL] [exception] failed to download [" + url + "] ..."



						# if its not a gfycat url we just fail
						else:
							print "[FAIL] [403] forbidden to download [" + url + "] ..."

					# server is cooked...
					elif r.status_code == 500:
						print "[FAILED] [500] failed to download [" + url + "] server down..."

				# my programming is cooked...
				except:
					print "[EPIC FAIL] [exception] failed to download [" + url + "] ..."

				# add wun
				index += 1
				print ""

'''
Converts gifs in a certain directory into mp4's
'''
def convert_gif_to_mp4():
	for filename in os.listdir(directory_to_save + "/"):
		if filename.endswith(".gif"):
			clip = mp.VideoFileClip(os.path.join(directory, filename))
			clip.write_videofile(os.path.join(directory, filename[:-3] + "mp4"))
			print "Converted " + os.path.join(directory, filename)
			continue


# Run it bruv
main()
convert_gif_to_mp4()






