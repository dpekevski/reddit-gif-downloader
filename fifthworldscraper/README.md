SUBREDDIT GIF DOWNLOADER:

1. start a virtual environment

`$ pip install virtualenv`
`$ cd reddit-gifs`
`$ virtualenv reddit-gifs`
`$ source bin/activate`

2. install all da libraries from my **requirements.txt**

`$ pip install reddit-gifs/requirements.txt`

3. Add your subreddit pages you want to scrap in **redditbot.py**

```
# add

start_urls = [
	... add me here ...
]

```

4. Run the scraper

`$ scrapy crawl reddit-bot`

5. Check that you have a generated csv file

`$ cd reddit_gifs/fifthworldscraper/fifthworldscraper`
`$ ls | grep csv`

6. Open the downloader and add the directory you want to download your gifs/mp4s too...

`$ vim downloadgifs.py`

7. Run the downloader

`$ python downloadgifs.py`

8. Check that your directory has the mp4s.

Note: it will have some leftover gifs that have been converted to mp4s already - you can delete them if you like
Double Note: this script took around ~5-10mins for ~80 gifs of varying sizes.